//
//  ViewController.swift
//  SwitchButton-ios-Animation
//
//  Created by Ryan Caracciolo on 4/2/20.
//  Copyright © 2020 Logix Applications. All rights reserved.
// 

import UIKit

class ViewController: UIViewController {
    
    var bOne = UIButton()
    var rightConst = NSLayoutConstraint()
    
    var bTwo = UIButton()
    var ca1 = NSLayoutConstraint()
    var ca2 = NSLayoutConstraint()
    var bThree = UIButton()
    var cb1 = NSLayoutConstraint()
    var cb2 = NSLayoutConstraint()
    var bFour = UIButton()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = .gray
        
        view.addSubview(bOne)
        bOne.setImage(UIImage(named: "swap"), for: .normal)
        bOne.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        bOne.translatesAutoresizingMaskIntoConstraints = false
        bOne.backgroundColor = .white
        bOne.layer.cornerRadius = 25
        
        bOne.widthAnchor.constraint(equalToConstant: 50).isActive = true
        bOne.heightAnchor.constraint(equalToConstant: 50).isActive = true
        rightConst = bOne.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -100)
        rightConst.isActive = true
        bOne.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        
        bOne.addTarget(self, action: #selector(handleTouch), for: .touchUpInside)
        
        view.addSubview(bTwo)
        bTwo.translatesAutoresizingMaskIntoConstraints = false
        bTwo.backgroundColor = .white
        bTwo.layer.cornerRadius = 12.5
        bTwo.alpha = 0
        
        bTwo.widthAnchor.constraint(equalToConstant: 25).isActive = true
        bTwo.heightAnchor.constraint(equalToConstant: 25).isActive = true
        ca1 = bTwo.rightAnchor.constraint(equalTo: bOne.leftAnchor, constant: 37.5)
        ca1.isActive = true
        ca2 = bTwo.bottomAnchor.constraint(equalTo: bOne.topAnchor, constant: 37.5)
        ca2.isActive = true
        
        view.addSubview(bThree)
        bThree.translatesAutoresizingMaskIntoConstraints = false
        bThree.backgroundColor = .white
        bThree.layer.cornerRadius = 12.5
        bThree.alpha = 0
        
        bThree.widthAnchor.constraint(equalToConstant: 25).isActive = true
        bThree.heightAnchor.constraint(equalToConstant: 25).isActive = true
        cb1 = bThree.rightAnchor.constraint(equalTo: bOne.leftAnchor, constant: 37.5)
        cb1.isActive = true
        cb2 = bThree.topAnchor.constraint(equalTo: bOne.bottomAnchor, constant: -37.5)
        cb2.isActive = true
        
        
        view.addSubview(bFour)
        bFour.translatesAutoresizingMaskIntoConstraints = false
        bFour.backgroundColor = .white
        bFour.layer.cornerRadius = 12.5
        bFour.alpha = 0
        
        bFour.widthAnchor.constraint(equalToConstant: 25).isActive = true
        bFour.heightAnchor.constraint(equalToConstant: 25).isActive = true
        bFour.rightAnchor.constraint(equalTo: bThree.leftAnchor, constant: 0).isActive = true
        bFour.centerYAnchor.constraint(equalTo: bOne.centerYAnchor, constant: 0).isActive = true
    }
    
    @objc func handleTouch() {
        if(self.rightConst.constant == -120) {
            self.rightConst.constant = -100
            ca1.constant = 37.5
            ca2.constant = 37.5
            cb1.constant = 37.5
            cb2.constant = -37.5
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
                self.bTwo.alpha = 0
                self.bThree.alpha = 0
                self.bFour.alpha = 0
                self.bOne.transform = CGAffineTransform(rotationAngle: 0)
            }
        }
        else {
            self.rightConst.constant = -120
            ca1.constant = 5
            ca2.constant = 5
            cb1.constant = 5
            cb2.constant = -5
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
                self.bTwo.alpha = 1
                self.bThree.alpha = 1
                self.bFour.alpha = 1
                self.bOne.transform = CGAffineTransform(rotationAngle: -3.1415/2)
            }
        }
    }
    
    
}
